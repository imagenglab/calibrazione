#!/usr/bin/python
import sys
import math
from skimage import io

import numpy as np

from skimage.morphology import dilation, square, opening, erosion, disk

from scipy import ndimage



#numero derivata

n =3
print('Elaborazione in corso...')


image= io.imread(sys.argv[1], as_grey=True )*255   # era normalizzato a 1, l'ho normalizzato a 255

n_col, n_righe = image.shape

out= np.zeros((n_col, n_righe), dtype=np.uint8)   #uint8 = Unsigned integer (0 to 255)
soglia_modulo=343



def intervallo(r, c):
    if c > n and c < n_col-(n+1) and r > n and r < n_righe-(n+1):
	return True
    else:
	return False

#calcolo della derivata x          PARZIALE
def dex(image, r, c,  default=1.0):
    
        dx= image[c+n, r]-image[c-n, r]
        if dx == 0:
	    return default
	else:
	    return float(dx)
   	
 


#calcolo della derivata y            PARZIALE

def dey(image, r, c,  default=1.0):
   
        dy= image[c, r-n]-image[c, r+n]
        if dy == 0:
	    return default
	else:
	    return float(dy)
    

#calcolo del modulo

def modulo(derx, dery):
    default=1.0
    if derx!=default and dery!=default:
       return math.sqrt(math.pow(derx, 2.0) + math.pow(dery, 2.0)) 
    else:
       return default

def valori(image,soglia_modulo):
    n_col, n_righe = image.shape

    for c in range(n_col):
        for r in range(n_righe):
	    if intervallo(r,c)  is True:
      		
       	 	modul= modulo(dex(image,r,c),dey(image,r,c))
                
		if  modul > soglia_modulo:
                    
		    out[c][r]=255
    return out







out_img= valori(image,soglia_modulo)
io.imsave('risultat.jpg', out_img)

# kernel
matr = square(3, dtype=np.uint8)
matr2= disk( 3, dtype=np.uint8)
cont=0

# operazione di closing

# dilatazione

while cont<3:
    out_img = dilation(out_img, matr2)
    cont+=1

dil1= ndimage.median_filter(out_img, 10)

#erosione

while cont<6:
    dil1 = erosion(dil1, matr)
    cont+=1

io.imsave('dilat.jpg' , dil1)

from skimage import measure
#regionprops ci da le regioni dell'imm labellata
#mesure e' il modulo che contiene le funzioni di misure tipo label e regionprops

regions = measure.regionprops(measure.label(dil1))
out = np.zeros((n_col, n_righe), dtype=np.uint8)
calibrazione = list()                       #contiene i valori delle coordinate dei punti

for r in regions:
    y1, x1, y4, x4 = r.bbox
    x = int((x1 + x4) / 2)
    y = int((y1 + y4) / 2)
    out[y, x] = 255
    calibrazione.append((x, y))  # append aggiunge alla fine una tupla x y

io.imsave('finale.png', out)
print('Processo ultimato.')
