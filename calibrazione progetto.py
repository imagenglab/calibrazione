#!/usr/bin/env python

import numpy as np
import cv2
from glob import glob

if __name__ == '__main__':

    img_input = cv2.imread("GOPR0059.JPG") #immagine di esempio
    img_maschere = 'imgs/GOPR00*.jpg'  # set di immagini
    img_path = glob(img_maschere)

# parametri iniziali
    square_size = float(1.0)
    pattern_size = (9, 6)
    pattern_points = np.zeros((np.prod(pattern_size), 3), np.float32)
    pattern_points[:, :2] = np.indices(pattern_size).T.reshape(-1, 2)
    pattern_points *= square_size
    obj_points = []
    img_points = []
    h, w = 0, 0
    img_names_undistort = []
    
    for fn in img_path:
        img = cv2.imread(fn, 0)
        if img is None:
            continue
        h, w = img.shape[:2]
        found, corners = cv2.findChessboardCorners(img, pattern_size)
        if found:
            term = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_COUNT, 30, 0.1)
            cv2.cornerSubPix(img, corners, (5, 5), (-1, -1), term)

        if not found:
            continue

        img_points.append(corners.reshape(-1, 2))
        obj_points.append(pattern_points)


    # K=matrice d=coefficienti di distorsione
    rms, K, d, rvecs, tvecs = cv2.calibrateCamera(obj_points, img_points, (w, h), None, None)

    print(K)
    print(d)
    # impedisco traslazione
    d[0][2:5] = 0
    print(d)
    h, w = img.shape[:2]
    newcamera, roi = cv2.getOptimalNewCameraMatrix(K, d, (w,h), 0)
    newimg = cv2.undistort(img_input, K, d, None, newcamera)
    
    cv2.imwrite("original.jpg", img_input)
    cv2.imwrite("undistorted.jpg", newimg)